#ifndef ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotate_image(struct image image, int angle);

#endif
